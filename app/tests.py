from django.test import TestCase
from app.models import News


class NewsTestCase(TestCase):
    def setUp(self):
        News.objects.create(title="test", body="Lorem ipsum dolor sit amet....")

    def test_news_creation(self):
        news = News.objects.first()
        self.assertEqual(news.title, 'test')
